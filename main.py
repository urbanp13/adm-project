# author: Petr Urban
# e-mail: urbanp13@fit.cvut.cz
# content: testing single imputation methods and its effect on classification error

import pandas as pd
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
from IPython.core.display import display
from sklearn.cross_validation import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn import neighbors
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier

from sklearn import linear_model
from sklearn.neural_network import MLPRegressor
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import make_scorer
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score


# CONSTANTS
SPLIT_RATIO = 0.25
SHOW_CHARTS = False
SEED = 5248 #pro opakovatelnost vysledku
CV = 5  #pro snizeni doby behu mozno snizit

#pd.set_option('display.max_columns', None)

#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

def rmsle(real, predicted):
    sum=0.0
    i = 0
    for x in real:
        if predicted[i]<0 or x < 0: #check for negative values
            continue
        p = np.log(predicted[i]+1)
        r = np.log(x+1)
        sum = sum + (p - r)**2
        i = i + 1
    return abs(sum/len(predicted))**0.5

rmsle_scorer = make_scorer(rmsle, greater_is_better=False)

def percent(real, predicted):
    if (len(predicted) != len(real)):
        print (len(predicted), len(real))#nemelo by nastat
    sum = 0.0
    i = 0
    for x in real:
        if (x == 0):
            continue
        diff = abs(x - predicted[i])
        sum += diff/x
        i = i + 1
    return sum/len(predicted)

percent_scorer = make_scorer(percent, greater_is_better=False)


def prepare_adult(df):
    df = df.drop(columns=['fnlwgt', 'capital-gain', 'capital-loss', 'native-country'])

    income = df['income']
    df = df.drop(columns=['income'])
    obj_df = df.select_dtypes(include=['object']).copy()
    df = df.drop(columns=obj_df)

    dummies = pd.get_dummies(obj_df, columns=obj_df.columns, prefix=obj_df.columns)
    df = pd.DataFrame.combine_first(df,dummies)


    x = df.values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled, columns=df.columns)
    df['income'] = income
    return df


def prepare_home(df):
    df = df[['price', 'bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors',
             'waterfront', 'view', 'condition', 'grade', 'yr_built', 'yr_renovated']]
    df = df.drop(columns=['waterfront', 'view'])

    #v pripade, ze dum nebyl rekonstruovan, je rok rekonstrukce nastaven na rok postaveni
    df['yr_renovated'] = np.where(df['yr_renovated'] == 0, df['yr_built'], df['yr_renovated'])
    #binning
    df['bathrooms'] = np.digitize(df['bathrooms'], bins=[1, 2, 3])
    df['floors'] = np.digitize(df['floors'], bins=[1, 2, 3])
    df['sqft_lot'] = np.digitize(df['sqft_lot'], bins=[5000, 10000, 20000])

    price = df['price']
    x = df.drop(columns=['price']).values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled, columns=df.drop(columns=['price']).columns)
    df['price'] = price

    return df


#https://www.kaggle.com/epa/fuel-economy/data
#vyber sloupcu
def prepare_vehicles(df):
    df = df[['Year', 'Class', 'Drive', 'Transmission', 'Engine Cylinders',
     'Engine Displacement', 'Fuel Type 1', 'Fuel Economy Score', '2D Passenger Volume', '2D Luggage Volume',
     '4D Passenger Volume', '4D Luggage Volume', 'Hatchback Passenger Volume', 'Hatchback Luggage Volume',
     'Start Stop Technology', 'Combined Gasoline Consumption (CD)', 'Combined MPG (FT1)', 'Combined MPG (FT2)']]
    #spojeni dat o prostorech pro posadku a zavazadla
    df["Passenger Volume"] = df[["2D Passenger Volume", "4D Passenger Volume", "Hatchback Passenger Volume"]].max(axis=1)
    df["Luggage Volume"] = df[["2D Luggage Volume", "4D Luggage Volume", "Hatchback Passenger Volume"]].max(
        axis=1)

    #predpoklad, ze pokud start stop neni uveden, tak ho nema
    #df = df.ix[df['Start Stop Technology'] == 'NaN', 'Start Stop Technology'] = 'N'
    #df['Start Stop Technology'] = np.where(df['Start Stop Technology'] == 'NaN', 'N', df['Start Stop Technology'])
    df['Start Stop Technology'].fillna('N', inplace=True)
    df['Start Stop Technology'] = np.where(df['Start Stop Technology'] == 'Y', 1, 0)
    #podobne u nahonu je v pripade nevyplneni predpoklad 2WD (jakozto zakladni), prumerovat nema smysl
    df['Drive'].fillna('2-Wheel Drive', inplace=True)


    eng_cyl = df["Engine Cylinders"]

    df = df.drop(columns=['Engine Cylinders'])
    obj_df = df.select_dtypes(include=['object']).copy()
    df = df.drop(columns=obj_df)

    dummies = pd.get_dummies(obj_df, columns=obj_df.columns, prefix=obj_df.columns)
    df = pd.DataFrame.combine_first(df, dummies)

    df.dropna(inplace=True)
    indices_to_keep = ~df.isin([np.nan, np.inf, -np.inf]).any(1)
    df = df[indices_to_keep]

    x = df.values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled, columns=df.columns)
    df["Engine Cylinders"] = eng_cyl

    df.dropna(inplace=True)
    indices_to_keep = ~df.isin([np.nan, np.inf, -np.inf]).any(1)
    df = df[indices_to_keep]

    return df

def prepare_auto_mpg(df):
    #nazvy jsou unikatni, k nicemu
    df = df.drop(columns=['car_name'])
    df = df.drop(df[df["horsepower"] == '?'].index)
    mpg = df["mpg"]
    x = df.drop(columns=["mpg"]).values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled, columns=df.drop(columns=['mpg']).columns)
    df["mpg"] = mpg
    df.dropna(inplace=True)
    indices_to_keep = ~df.isin([np.nan, np.inf, -np.inf]).any(1)
    df = df[indices_to_keep]
    return df

def prepare_letter(df):
    letter = df["letter"]
    x = df.drop(columns=["letter"]).values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled, columns=df.drop(columns=['letter']).columns)
    df["letter"] = letter
    return df


def prepare_wine(df):
    quality = df['quality']
    x = df.drop(columns=['quality']).values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled, columns=df.drop(columns=['quality']).columns)
    df['quality'] = quality
    return df


#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#vzdy vratit train, test bez labelu a imputovaneho atributu, label a spravne hodnoty imp atributu

def split_dataset(df, label, missing):
    train, test = train_test_split(df, test_size=SPLIT_RATIO, random_state=SEED)
    Xtrain = train.drop([label], axis = 1, errors = 'ignore')
    ytrain = train[label]
    Xtrain_missing = Xtrain.drop([missing], axis = 1, errors = 'ignore')
    ytrain_missing = train[missing]
    Xtest = test.drop(columns=[label, missing], axis = 1, errors = 'ignore')
    Xtest_no_miss = test.drop(columns=[label], axis=1, errors='ignore')
    ytest = test[label]
    ytest_missing = test[missing]

    return Xtrain,ytrain,Xtrain_missing,ytrain_missing,Xtest,ytest,ytest_missing,Xtest_no_miss



#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
def impute_continuous_tree (missing_column_name,Xtrain_missing,ytrain_missing,Xtest,ytest_missing):
    values_list = list(range(1, 15))
    cv_scores = []

    for i in values_list:
        dtr = tree.DecisionTreeRegressor(max_depth=i, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(dtr, Xtrain_missing, ytrain_missing, cv=CV, scoring=rmsle_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal max_depth is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('max_depth')
        plt.ylabel('RMSLE')
        plt.show()

    dtr = tree.DecisionTreeRegressor(max_depth=opt, min_samples_leaf=5, random_state=SEED)
    dtr.fit(Xtrain_missing, ytrain_missing)
    ypredict = dtr.predict(Xtest)
    Xtest[missing_column_name] = ypredict

    print("Impute tree - Percent: ", percent(ytest_missing, ypredict))
    print("Impute tree - RMSLE: ", rmsle(ytest_missing, ypredict))
    return Xtest

# -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
def impute_continuous_forest(missing_column_name, Xtrain_missing, ytrain_missing, Xtest, ytest_missing):
    values_list = list(range(5, 100, 5))
    cv_scores = []

    for i in values_list:
        rfr = RandomForestRegressor(n_estimators=i, max_depth=8, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(rfr, Xtrain_missing, ytrain_missing, cv=CV, scoring=rmsle_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal n_estimators is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('n_estimators')
        plt.ylabel('RMSLE')
        plt.show()

    rfr = RandomForestRegressor(n_estimators=opt, max_depth=8, min_samples_leaf=5, random_state=SEED)
    rfr.fit(Xtrain_missing, ytrain_missing)
    ypredict = rfr.predict(Xtest)
    Xtest[missing_column_name] = ypredict

    print("Impute forest - Percent: ", percent(ytest_missing, ypredict))
    print("Impute forest - RMSLE: ", rmsle(ytest_missing, ypredict))
    return Xtest

#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
def impute_continuous_knn (missing_column_name,Xtrain_missing,ytrain_missing,Xtest,ytest_missing):
    values_list = list(range(1, 20, 2))
    cv_scores = []

    for i in values_list:
        knn = neighbors.KNeighborsRegressor(n_neighbors=i)
        scores = cross_val_score(knn, Xtrain_missing, ytrain_missing, cv=CV, scoring=rmsle_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal k is {0} (score {1:.4f})".format(opt, max(cv_scores)))

    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('k')
        plt.ylabel('RMSLE')
        plt.show()

    knn = neighbors.KNeighborsRegressor(n_neighbors=opt)
    knn.fit(Xtrain_missing, ytrain_missing)
    ypredict = knn.predict(Xtest)
    Xtest[missing_column_name] = ypredict
    print("Impute knn - Percent: ", percent(ytest_missing, ypredict))
    print("Impute knn - RMSLE: ", rmsle(ytest_missing, ypredict))
    return Xtest



#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
def impute_continuous_linreg (missing_column_name,Xtrain_missing,ytrain_missing,Xtest,ytest_missing):
    lr = linear_model.LinearRegression()
    lr.fit(Xtrain_missing, ytrain_missing)

    ypredict = lr.predict(Xtest)
    '''res = pd.DataFrame(np.matrix([ypredict, ytest_missing, ypredict - ytest_missing]).T)
    res.columns = ['predict', 'real', 'diff']
    print (res)'''

    Xtest[missing_column_name] = ypredict

    print("Impute linreg - Percent: ", percent(ytest_missing, ypredict))
    print("Impute linreg - RMSLE: ", rmsle(ytest_missing, ypredict))
    return Xtest

#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
def impute_continuous_mlp (missing_column_name,Xtrain_missing,ytrain_missing,Xtest,ytest_missing):
    values_list = [x / 100 for x in list(range(1, 20, 4))]
    cv_scores = []

    for i in values_list:
        mlp = MLPRegressor(hidden_layer_sizes=(10,), learning_rate_init=i, max_iter=2000,
                           random_state=SEED, early_stopping=True)
        scores = cross_val_score(mlp, Xtrain_missing, ytrain_missing, cv=CV)#, scoring=rmsle_scorer)

        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal learning_rate is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('learning_rate')
        plt.ylabel('RMSLE')
        plt.show()

    mlp = MLPRegressor(hidden_layer_sizes=(10,), learning_rate_init=opt, max_iter=1000,
                       random_state=SEED, early_stopping=True)
    mlp.fit(Xtrain_missing, ytrain_missing)
    ypredict = mlp.predict(Xtest)
    Xtest[missing_column_name] = ypredict

    print("Impute mlp - Percent: ", percent(ytest_missing, ypredict))
    print("Impute mlp - RMSLE: ", rmsle(ytest_missing, ypredict))
    return Xtest

#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
def impute_continuous_mean (missing_column_name,Xtrain,Xtest,ytest_missing):
    mean = Xtrain[missing_column_name].mean()

    Xtest[missing_column_name] = mean
    return Xtest

#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
def discrete_label_knn (label_name,Xtrain,ytrain,Xtest,ytest):
    values_list = list(range(1, 20, 2))
    cv_scores = []

    for i in values_list:
        knn = neighbors.KNeighborsClassifier(n_neighbors=i)
        scores = cross_val_score(knn, Xtrain, ytrain, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal k is {0} (score {1:.4f})".format(opt, max(cv_scores)))

    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('k')
        plt.ylabel('Accuracy')
        plt.show()

    knn = neighbors.KNeighborsClassifier(n_neighbors=opt)
    knn.fit(Xtrain, ytrain)
    ypredict = knn.predict(Xtest)

    acc = accuracy_score(ytest, ypredict)
    print("Accuracy pri pouziti nejblizsich sousedu je {}".format(acc))


def continuous_label_knn(label_name, Xtrain, ytrain, Xtest, ytest):
    values_list = list(range(1, 20, 2))
    cv_scores = []

    for i in values_list:
        knn = neighbors.KNeighborsRegressor(n_neighbors=i)
        scores = cross_val_score(knn, Xtrain, ytrain, cv=CV, scoring=rmsle_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal k is {0} (score {1:.4f})".format(opt, max(cv_scores)))

    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('k')
        plt.ylabel('RMSLE')
        plt.show()

    knn = neighbors.KNeighborsRegressor(n_neighbors=opt)
    knn.fit(Xtrain, ytrain)
    ypredict = knn.predict(Xtest)
    rmslerr = rmsle(ytest, ypredict)
    print("RMSLE pri pouziti nejblizsich sousedu je {}".format(rmslerr))

def discrete_label_tree(label_name, Xtrain, ytrain, Xtest, ytest):
    values_list = list(range(1, 15))
    cv_scores = []

    for i in values_list:
        dtr = tree.DecisionTreeClassifier(max_depth=i, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(dtr, Xtrain, ytrain, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal max_depth is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('max_depth')
        plt.ylabel('Accuracy')
        plt.show()

    dtr = tree.DecisionTreeClassifier(max_depth=opt, min_samples_leaf=5, random_state=SEED)
    dtr.fit(Xtrain, ytrain)
    ypredict = dtr.predict(Xtest)

    acc = accuracy_score(ytest, ypredict)
    print("Accuracy pri pouziti stromu je {}".format(acc))

def continuous_label_tree(label_name, Xtrain, ytrain, Xtest, ytest):
    values_list = list(range(1, 15))
    cv_scores = []

    for i in values_list:
        dtr = tree.DecisionTreeRegressor(max_depth=i, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(dtr, Xtrain, ytrain, cv=CV, scoring=rmsle_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal max_depth is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('max_depth')
        plt.ylabel('RMSLE')
        plt.show()

    dtr = tree.DecisionTreeRegressor(max_depth=opt, min_samples_leaf=5, random_state=SEED)
    dtr.fit(Xtrain, ytrain)
    ypredict = dtr.predict(Xtest)
    rmslerr = rmsle(ytest, ypredict)
    print("RMSLE pri pouziti stromu je {}".format(rmslerr))

def discrete_label_forest(label_name, Xtrain, ytrain, Xtest, ytest):
    values_list = list(range(5, 100, 5))
    cv_scores = []

    for i in values_list:
        rfr = RandomForestClassifier(n_estimators=i, max_depth=8, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(rfr, Xtrain, ytrain, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal n_estimators is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('n_estimators')
        plt.ylabel('Accuracy')
        plt.show()

    rfr = RandomForestClassifier(n_estimators=opt, max_depth=8, min_samples_leaf=5, random_state=SEED)
    rfr.fit(Xtrain, ytrain)
    ypredict = rfr.predict(Xtest)

    acc = accuracy_score(ytest, ypredict)
    print("Accuracy pri pouziti nahodnych lesu je {}".format(acc))


def continuous_label_forest(label_name, Xtrain, ytrain, Xtest, ytest):
    values_list = list(range(5, 100, 5))
    cv_scores = []

    for i in values_list:
        rfr = RandomForestRegressor(n_estimators=i, max_depth=8, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(rfr, Xtrain, ytrain, cv=CV, scoring=rmsle_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal n_estimators is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('n_estimators')
        plt.ylabel('RMSLE')
        plt.show()

    rfr = RandomForestRegressor(n_estimators=opt, max_depth=8, min_samples_leaf=5, random_state=SEED)
    rfr.fit(Xtrain, ytrain)
    ypredict = rfr.predict(Xtest)
    rmslerr = rmsle(ytest, ypredict)
    print ("RMSLE pri pouziti nahodnych lesu je {}".format(rmslerr))

def discrete_label_mlp(label_name, Xtrain, ytrain, Xtest, ytest):
    values_list = [x / 100 for x in list(range(1, 20, 4))]
    cv_scores = []

    for i in values_list:
        mlp = MLPClassifier(hidden_layer_sizes=(10,), learning_rate_init=i, max_iter=2000,
                           random_state=SEED, early_stopping=True)
        scores = cross_val_score(mlp, Xtrain, ytrain, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal learning_rate is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('learning_rate')
        plt.ylabel('Accuracy')
        plt.show()

    mlp = MLPClassifier(hidden_layer_sizes=(100,), learning_rate_init=opt, max_iter=2000,
                       random_state=SEED, early_stopping=True)
    mlp.fit(Xtrain, ytrain)

    ypredict = mlp.predict(Xtest)

    acc = accuracy_score(ytest, ypredict)
    print("Accuracy pri pouziti mlp je {}".format(acc))


def continuous_label_mlp(label_name, Xtrain, ytrain, Xtest, ytest):
    values_list = [x / 100 for x in list(range(1, 20, 4))]
    cv_scores = []

    for i in values_list:
        mlp = MLPRegressor(hidden_layer_sizes=(10,), learning_rate_init=i, max_iter=2000,
                           random_state=SEED, early_stopping=True)
        scores = cross_val_score(mlp, Xtrain, ytrain, cv=CV, scoring=rmsle_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    print("Optimal learning_rate is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('learning_rate')
        plt.ylabel('RMSLE')
        plt.show()

    mlp = MLPRegressor(hidden_layer_sizes=(100,), learning_rate_init=opt, max_iter=2000,
                       random_state=SEED, early_stopping=True)
    mlp.fit(Xtrain, ytrain)

    ypredict = mlp.predict(Xtest)
    rmslerr = rmsle(ytest, ypredict)
    print ("RMSLE pri pouziti mlp je {}".format(rmslerr))


def continuous_label_linreg(label_name, Xtrain, ytrain, Xtest, ytest):
    lr = linear_model.LinearRegression()
    lr.fit(Xtrain, ytrain)

    ypredict = lr.predict(Xtest)
    rmslerr = rmsle(ytest, ypredict)
    print ("RMSLE pri pouziti linearni regrese je {}".format(rmslerr))


def print_info(dataset_name,Xtrain,ytrain,Xtrain_missing,ytrain_missing,Xtest,ytest,ytest_missing,Xtest_no_miss):
    print("DATASET", dataset_name)
    print('Xtrain shape:', Xtrain.shape)
    print('ytrain shape:', ytrain.shape)
    print('Xtrain_missing shape:', Xtrain_missing.shape)
    print('ytrain_missing shape:', ytrain_missing.shape)
    print('Xtest shape:', Xtest.shape)
    print('ytest shape:', ytest.shape)
    print('ytest_missing shape:', ytest_missing.shape)
    print('Xtest_no_miss shape:', Xtest_no_miss.shape)

'''
#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#                                                 MAIN
#-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

files = ["data/adult/adult.data.txt", "data/home/home_data.csv", "data/vehicles/vehicles.csv",
         "data/auto-mpg/auto-mpg.data.txt", "data/letter/letter-recognition.data.txt", "data/wine/winequality-red.csv"]
labels = ["income", "price", "Engine Cylinders", "mpg", "letter", "quality"]
continuous_label = [False, True, False, True, False, False]
missing = ["hours-per-week", "sqft_living", "Fuel Economy Score", "displacement", "onpix", "fixed acidity" ]
functions_imp = [impute_continuous_mean,impute_continuous_knn,impute_continuous_tree,impute_continuous_forest,impute_continuous_mlp,impute_continuous_linreg]
functions_con = [continuous_label_knn,continuous_label_tree,continuous_label_forest,continuous_label_mlp,continuous_label_linreg]
functions_dis = [discrete_label_knn,discrete_label_tree,discrete_label_forest,discrete_label_mlp]
functions_prep = [prepare_adult,prepare_home,prepare_vehicles,prepare_auto_mpg,prepare_letter,prepare_wine]
dataframes = []

for file in files:
    if file == "data/auto-mpg/auto-mpg.data.txt":
        dataframes.append(pd.read_csv(file, delim_whitespace=True))
    elif file == "data/wine/winequality-red.csv":
        dataframes.append(pd.read_csv(file, delimiter=";"))
    else:
        dataframes.append(pd.read_csv(file))

#zvolte cislo datasetu 0-5
DATASET = 2
#zvolte, jestli chcete chybejici data ignorovat(0), pouzit dataset bez chybejicich dat(1) nebo zkusit data imputovat(2)
METHOD = 2
#zvolte imputation metodu (pouzije se pouze pokud METHOD == 2) [0-prumer, 1-knn, 2-strom, 3-nahodny les, 4-neuronova sit, 5-linarni regrese]
IMPUTE = 2
#zvolte algoritmus pro klasifikaci (pouzito na diskretni labely) [0-knn, 1-strom, 2-nahodny les, 3-neuronova sit]
CLASS = 2
#zvolte algoritmus pro regresi (pouzito na spojitou cilovou promennou) [0-knn, 1-strom, 2-nahodny les, 3-neuronova sit, 4-linarni regrese]
REG = 2


dataframes[DATASET] = functions_prep[DATASET](dataframes[DATASET])
Xtrain,ytrain,Xtrain_missing,ytrain_missing,Xtest,ytest,ytest_missing,Xtest_no_miss = split_dataset(dataframes[DATASET], labels[DATASET], missing[DATASET])
print_info(files[DATASET],Xtrain,ytrain,Xtrain_missing,ytrain_missing,Xtest,ytest,ytest_missing,Xtest_no_miss)

if(METHOD == 0):
    if(continuous_label[DATASET]):
        functions_con[REG](labels[DATASET], Xtrain_missing, ytrain, Xtest, ytest)
    else:
        functions_dis[CLASS](labels[DATASET], Xtrain_missing, ytrain, Xtest, ytest)
elif(METHOD == 1):
    if(continuous_label[DATASET]):
        functions_con[REG](labels[DATASET], Xtrain, ytrain, Xtest_no_miss, ytest)
    else:
        functions_dis[CLASS](labels[DATASET], Xtrain, ytrain, Xtest_no_miss, ytest)
elif(METHOD == 2):
    Xtest_imputed = functions_imp[IMPUTE](missing[DATASET], Xtrain_missing,ytrain_missing,Xtest,ytest_missing)
    if(continuous_label[DATASET]):
        functions_con[REG](labels[DATASET], Xtrain, ytrain, Xtest_imputed, ytest)
    else:
        functions_dis[CLASS](labels[DATASET], Xtrain, ytrain, Xtest_imputed, ytest)

'''












def train_knn_classifier(X, y):
    values_list = list(range(1, 20, 2))
    cv_scores = []

    for i in values_list:
        knn = neighbors.KNeighborsClassifier(n_neighbors=i)
        scores = cross_val_score(knn, X, y, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    #print("Optimal k is {0} (score {1:.4f})".format(opt, max(cv_scores)))

    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('n_neighbors')
        plt.ylabel('Accuracy')
        plt.show()

    knn = neighbors.KNeighborsClassifier(n_neighbors=opt)
    knn.fit(X, y)
    return knn

def train_tree_classifier(X, y):
    values_list = list(range(1, 15))
    cv_scores = []

    for i in values_list:
        dtr = tree.DecisionTreeClassifier(max_depth=i, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(dtr, X, y, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    #print("Optimal max_depth is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('max_depth')
        plt.ylabel('Accuracy')
        plt.show()

    dtr = tree.DecisionTreeClassifier(max_depth=opt, min_samples_leaf=5, random_state=SEED)
    dtr.fit(X, y)
    return dtr

def train_forest_classifier(X, y):
    values_list = list(range(5, 70, 10))
    cv_scores = []

    for i in values_list:
        rfr = RandomForestClassifier(n_estimators=i, max_depth=8, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(rfr, X, y, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    #print("Optimal n_estimators is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('n_estimators')
        plt.ylabel('Accuracy')
        plt.show()

    rfr = RandomForestClassifier(n_estimators=opt, max_depth=8, min_samples_leaf=5, random_state=SEED)
    rfr.fit(X, y)
    return rfr


def train_mlp_classifier(X, y):
    values_list = [x / 100 for x in list(range(1, 20, 4))]
    cv_scores = []

    for i in values_list:
        mlp = MLPClassifier(hidden_layer_sizes=(10,), learning_rate_init=i, max_iter=2000,
                           random_state=SEED, early_stopping=True)
        scores = cross_val_score(mlp, X, y, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    #print("Optimal learning_rate is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('learning_rate')
        plt.ylabel('Accuracy')
        plt.show()

    mlp = MLPClassifier(hidden_layer_sizes=(100,), learning_rate_init=opt, max_iter=2000,
                       random_state=SEED, early_stopping=True)
    mlp.fit(X, y)
    return mlp


def train_knn_regressor(X, y):
    values_list = list(range(1, 20, 2))
    cv_scores = []

    for i in values_list:
        knn = neighbors.KNeighborsRegressor(n_neighbors=i)
        scores = cross_val_score(knn, X, y, cv=CV)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    #print("Optimal k is {0} (score {1:.4f})".format(opt, max(cv_scores)))

    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('n_neighbors')
        plt.ylabel('percent')
        plt.show()

    knn = neighbors.KNeighborsRegressor(n_neighbors=opt)
    knn.fit(X, y)
    return knn

def train_tree_regressor(X, y):
    values_list = list(range(1, 15))
    cv_scores = []

    for i in values_list:
        dtr = tree.DecisionTreeRegressor(max_depth=i, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(dtr, X, y, cv=CV, scoring=percent_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    #print("Optimal max_depth is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('max_depth')
        plt.ylabel('Accuracy')
        plt.show()

    dtr = tree.DecisionTreeRegressor(max_depth=opt, min_samples_leaf=5, random_state=SEED)
    dtr.fit(X, y)
    return dtr

def train_forest_regressor(X, y):
    values_list = list(range(5, 70, 10))
    cv_scores = []

    for i in values_list:
        rfr = RandomForestRegressor(n_estimators=i, max_depth=8, min_samples_leaf=5, random_state=SEED)
        scores = cross_val_score(rfr, X, y, cv=CV, scoring=rmsle_scorer)
        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    #print("Optimal n_estimators is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('n_estimators')
        plt.ylabel('RMSLE')
        plt.show()

    rfr = RandomForestRegressor(n_estimators=opt, max_depth=8, min_samples_leaf=5, random_state=SEED)
    rfr.fit(X, y)
    return rfr


def train_mlp_regressor(X, y):
    values_list = [x / 100 for x in list(range(1, 20, 4))]
    cv_scores = []

    for i in values_list:
        mlp = MLPRegressor(hidden_layer_sizes=(10,), learning_rate_init=i, max_iter=2000,
                           random_state=SEED, early_stopping=True)
        scores = cross_val_score(mlp, X, y, cv=CV)#, scoring=rmsle_scorer)

        cv_scores.append(scores.mean())

    opt = values_list[cv_scores.index(max(cv_scores))]
    #print("Optimal learning_rate is {0} (score {1:.4f})".format(opt, max(cv_scores)))
    if (SHOW_CHARTS):
        plt.plot(values_list, cv_scores)
        plt.xlabel('learning_rate')
        plt.ylabel('RMSLE')
        plt.show()

    mlp = MLPRegressor(hidden_layer_sizes=(10,), learning_rate_init=opt, max_iter=1000,
                       random_state=SEED, early_stopping=True)
    mlp.fit(X, y)
    return mlp






#------------------------------------------------------------------------------
DATASET = 0
CLASSIFIER = 0
REGRESSOR = 0


files_train = ["data_mf/wine/learning/wine_train.csv", "data_mf/cancer/learning/cancer_train.csv", "data_mf/magic/learning/magic_train.csv"]
files_test = ["data_mf/wine/learning/wine_test.csv", "data_mf/cancer/learning/cancer_test.csv", "data_mf/magic/learning/magic_test.csv"]
missing_folders = ["data_mf/wine/missing/nan/", "data_mf/cancer/missing/nan/", "data_mf/magic/missing/nan/"]
output_folders = ["output/wine/", "output/cancer/", "output/magic/"]
label = 'y'
prefix = "out_"
output_folder = output_folders[DATASET]


classifier_names = ["knn", "tree", "forest", "mlp"]
regressor_names = ["knn", "tree", "forest", "mlp"]


train_regressor = [train_knn_regressor, train_tree_regressor, train_forest_regressor, train_mlp_regressor]
train_classifier = [train_knn_classifier, train_tree_classifier, train_forest_classifier, train_mlp_classifier]

filename_train = files_train[DATASET]
train = pd.read_csv(filename_train, delimiter=";")

x = train.values
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)
train = pd.DataFrame(x_scaled, columns=train.columns)


Xtrain = train.drop([label], axis = 1, errors = 'ignore')
ytrain = train[label]

filename_test = files_test[DATASET]
test = pd.read_csv(filename_test, delimiter=";")
x = test.values
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)
test = pd.DataFrame(x_scaled, columns=test.columns)

Xtest = test.drop([label], axis = 1, errors = 'ignore')
ytest = test[label]

num_features = test.shape[1]-1
missing_folder = missing_folders[DATASET]


#training full datatset classifier
class_full = train_classifier[CLASSIFIER](Xtrain, ytrain)

full_file = missing_folders[DATASET] + "out_0_[].csv"
df = pd.read_csv(full_file, delimiter=";")
df = df.drop([label], axis=1, errors='ignore')

d = df.values
min_max_scaler = preprocessing.MinMaxScaler()
d_scaled = min_max_scaler.fit_transform(d)
df = pd.DataFrame(d_scaled, columns=df.columns)

for i in range(0,num_features):
    files = [filename for filename in os.listdir(missing_folder) if filename.startswith(prefix + str(i) + "_")]
    num_files = files.__len__()
    sum_acc = 0
    sum_acc_ignore = 0
    sum_acc_replace = 0

    sum_rmsle = 0.0
    for file in files:
        missing = pd.read_csv(missing_folder+file, delimiter=";")

        Xmissing = missing.drop([label], axis=1, errors='ignore')
        missing_columns = Xmissing.columns[Xmissing.isna().any()].tolist()
        Xmissing_replace = Xmissing.copy()
        Xmissing_replace = Xmissing_replace.fillna(0)
        Xmissing = Xmissing.drop(columns=missing_columns, axis=1, errors='ignore')

        x = Xmissing.values
        min_max_scaler = preprocessing.MinMaxScaler()
        x_scaled = min_max_scaler.fit_transform(x)
        Xmissing = pd.DataFrame(x_scaled, columns=Xmissing.columns)

        x = Xmissing_replace.values
        min_max_scaler = preprocessing.MinMaxScaler()
        x_scaled = min_max_scaler.fit_transform(x)
        Xmissing_replace = pd.DataFrame(x_scaled, columns=Xmissing_replace.columns)

        ymissing = missing[label]
        Xmissing_imputed = Xmissing.copy()

        Xtrain_ignore = Xtrain.drop(columns=missing_columns, axis=1, errors='ignore')



        # training classifier on reduced set of columns/features
        class_ignore = train_classifier[CLASSIFIER](Xtrain_ignore, ytrain)

        ypredict_ignore = class_ignore.predict(Xmissing)
        acc_ignore = accuracy_score(ymissing, ypredict_ignore)
        sum_acc_ignore += acc_ignore

        ypredict_replace = class_full.predict(Xmissing_replace)
        acc_replace = accuracy_score(ymissing, ypredict_replace)
        sum_acc_replace += acc_replace

        for col in missing_columns:
            Ximpute = Xtrain.drop(columns=missing_columns, axis=1, errors='ignore')
            yimpute = Xtrain[col]

            regressor = train_regressor[REGRESSOR](Ximpute, yimpute)
            missing_predict = regressor.predict(Xmissing)

            Xmissing_imputed[col] = missing_predict

        #JEDINA ZMENA - SPRAVNE POSKLADANI SLOUPCU
        Xmissing_imputed = Xmissing_imputed.reindex(Xtrain.columns, axis=1)

        file_rmsle = 0.0
        for col in missing_columns:
            col_rmsle = rmsle(df[[col]].values, Xmissing_imputed[[col]].values)
            file_rmsle += col_rmsle

        if(missing_columns.__len__() != 0):
            sum_rmsle += file_rmsle/missing_columns.__len__()

        ypredict = class_full.predict(Xmissing_imputed)
        acc = accuracy_score(ymissing, ypredict)
        sum_acc += acc

        # print("Accuracy pri num_missing = {}, pri {}-{}, pro soubor {} je {}".format(i, regressor_names[REGRESSOR], classifier_names[CLASSIFIER], file, acc))
        # print("Accuracy pri num_missing = {}, pri ignore-{}, pro soubor {} je {}".format(i, classifier_names[CLASSIFIER], file, acc_ignore))
        # print("Accuracy pri num_missing = {}, pri replace[-{}, pro soubor {} je {}".format(i, classifier_names[CLASSIFIER], file, acc_replace))
    final_rmsle = sum_rmsle/files.__len__()





    print("Prumerna accuracy num_missing = {}, pri {}-{} je {}/{} = {}".format(i, regressor_names[REGRESSOR], classifier_names[0],sum_acc, num_files, sum_acc/num_files))
    print("Prumerna accuracy num_missing = {}, pri ignore-{} je {}/{} = {}".format(i, classifier_names[CLASSIFIER], sum_acc_ignore, num_files, sum_acc_ignore / num_files))
    print("Prumerna accuracy num_missing = {}, pri replace-{} je {}/{} = {}\n\n".format(i, classifier_names[CLASSIFIER], sum_acc_replace, num_files, sum_acc_replace / num_files))
